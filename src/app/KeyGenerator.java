package app;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class KeyGenerator {
    private KeyPairGenerator keyGen;
    private KeyPair pair;
    private PrivateKey privateKey;
    private PublicKey publicKey;

    public KeyGenerator(int keylength) throws NoSuchAlgorithmException {
        this.keyGen = KeyPairGenerator.getInstance("RSA");
        this.keyGen.initialize(keylength);
    }

    public void createKeys() {
        this.pair = this.keyGen.generateKeyPair();
        this.privateKey = pair.getPrivate();
        this.publicKey = pair.getPublic();
    }

    public PrivateKey getPrivateKey() {
        return this.privateKey;
    }

    public PublicKey getPublicKey() {
        return this.publicKey;
    }

    public void writeToFile(String path, byte[] key) throws IOException {
        File f = new File(path);
        f.getParentFile().mkdirs();

        FileOutputStream fos = new FileOutputStream(f);
        fos.write(key);
        fos.flush();
        fos.close();
    }

    public static Key readFromFile(String name, boolean publicKey) {
        // default is privateKey
        String fileName = name + "_privateKey";

        if(publicKey) {
            fileName = name + "_publicKey";
        }

        try {
            byte[] keyBytes = Files.readAllBytes(new File("keys/" + fileName).toPath());

            KeyFactory kf = KeyFactory.getInstance("RSA");
            Key key = null;

            if(!publicKey) {
                PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
                key = kf.generatePrivate(spec);
            } else {
                X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
                key = kf.generatePublic(spec);
            }

            return key;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (NoSuchFileException e) {
            System.err.println("[INFO] Nie ma takiego klucza");
            return null;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}