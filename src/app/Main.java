package app;

import Algorithms.BruteForce;
import Algorithms.Greedy;
import Algorithms.RandomSearch;
import App.Instance;
import App.Item;
import App.Knapsack;
import App.Solution;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

public class Main {

    public static void main(String[] args) throws IOException {
        boolean stopProgram = false;
        Integer choose = null;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Instance instance = null;

        while(!stopProgram) {
            if(choose == null) {
                System.out.println("");
                System.out.println("1. Szyfrowanie/Rozszyfrowywanie plików");
                System.out.println("2. Rozwiązywanie problemu plecakowego");
                System.out.println("3. Generowanie kluczy");
                System.out.println("0. Koniec");
                System.out.print("Wybierz: ");

                choose = Integer.valueOf(reader.readLine());
            }

            switch (choose) {
                case 1: {
                    System.out.println("");
                    System.out.println("1. Szyfrowanie");
                    System.out.println("2. Rozszyfrowywanie");
                    System.out.println("0. Powrót");
                    System.out.print("Wybierz: ");

                    Integer option = Integer.valueOf(reader.readLine());

                    switch (option) {
                        case 1: {
                            System.out.print("Podaj nazwę pliku: ");
                            String inputFileName = reader.readLine();

                            System.out.print("Podaj nazwę pliku zaszyfrowanego (domyślnie encrypted_" + inputFileName + "): ");
                            String outputFileName = reader.readLine();

                            if(outputFileName.equals("")) {
                                outputFileName = "encrypted_" + inputFileName;
                            }

                            System.out.print("Podaj nazwę klucza: ");
                            String keyName = reader.readLine();

                            Key key = KeyGenerator.readFromFile(keyName, false);
                            String encryptedFile = Cryptography.encrypt(key, inputFileName, outputFileName);

                            if(key != null) {
                                String decryptedFile = Cryptography.encrypt(key, inputFileName, outputFileName);

                                if (decryptedFile != null) {
                                    System.out.println("[INFO] Zaszyfrowano plik (" + decryptedFile + ")");
                                } else {
                                    System.err.println("[INFO] Wystąpił błąd");
                                }
                            }

                            break;
                        }

                        case 2: {
                            System.out.print("Podaj nazwę pliku: ");
                            String inputFileName = reader.readLine();

                            System.out.print("Podaj nazwę pliku odszyfrowanego (domyślnie decrypted_" + inputFileName + "): ");
                            String outputFileName = reader.readLine();

                            if(outputFileName.equals("")) {
                                outputFileName = "decrypted_" + inputFileName;
                            }

                            System.out.print("Podaj nazwę klucza: ");
                            String keyName = reader.readLine();

                            Key key = KeyGenerator.readFromFile(keyName, true);

                            if(key != null) {
                                String decryptedFile = Cryptography.decrypt(key, inputFileName, outputFileName);

                                if (decryptedFile != null) {
                                    System.out.println("[INFO] Odszyfrowano plik (" + decryptedFile + ")");
                                } else {
                                    System.err.println("[INFO] Wystąpił błąd");
                                }
                            }

                            break;
                        }

                        default: {
                            choose = null;
                            break;
                        }
                    }

                    break;
                }

                case 2: {
                    System.out.println("");
                    System.out.println("1. Generowanie instancji");
                    System.out.println("2. BruteForce");
                    System.out.println("3. Greedy");
                    System.out.println("4. RandomSearch");
                    System.out.println("0. Powrót");
                    System.out.print("Wybierz: ");

                    Integer option = Integer.valueOf(reader.readLine());

                    switch (option) {
                        case 1: {
                            instance = Instance.generate(8, 50);

                            System.out.println("[LOG] Wygenerowano");
                            for(Item i : instance.getItemList()) {
                                System.out.println("\tWaga: " + i.getWeight() + "\tCena: " + i.getValue());
                            }
                            break;
                        }

                        case 2: {
                            Knapsack knapsack = new BruteForce(instance);
                            Solution solution = knapsack.findSolution();

                            System.out.println("[LOG] BruteForce - Przedmioty: " + solution.getItemList().size() + "\tWaga: " + solution.getItemsWeight() + "\tCena: " + solution.getItemsValue());
                            break;
                        }

                        case 3: {
                            Knapsack knapsack = new Greedy(instance);
                            Solution solution = knapsack.findSolution();

                            System.out.println("[LOG] Greedy - przedmioty: " + solution.getItemList().size() + "\twaga: " + solution.getItemsWeight() + "\tcena: " + solution.getItemsValue());
                            break;
                        }

                        case 4: {
                            Knapsack knapsack = new RandomSearch(instance);
                            Solution solution = knapsack.findSolution();

                            System.out.println("[LOG] RandomSearch - przedmioty: " + solution.getItemList().size() + "\twaga: " + solution.getItemsWeight() + "\tcena: " + solution.getItemsValue());
                            break;
                        }

                        default: {
                            choose = null;
                            break;
                        }
                    }
                    break;
                }

                case 3: {
                    System.out.print("Podaj nazwę: ");
                    String keyName = reader.readLine();

                    try {
                        KeyGenerator generator = new KeyGenerator(1024);
                        generator.createKeys();
                        generator.writeToFile("keys/" + keyName + "_publicKey", generator.getPublicKey().getEncoded());
                        generator.writeToFile("keys/" + keyName + "_privateKey", generator.getPrivateKey().getEncoded());
                        System.out.println("Wygenerowano (pliki: \"keys/" + keyName + "_publicKey\" oraz \"keys " + keyName + "_privateKey\"");
                    } catch (NoSuchAlgorithmException e) {
                        System.err.println(e.getMessage());
                    } catch (IOException e) {
                        System.err.println(e.getMessage());
                    }

                    choose = null;
                    break;
                }

                default: {
                    stopProgram = true;
                    break;
                }
            }

        }
    }
}
