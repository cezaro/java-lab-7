package app;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

public class Cryptography {
    public static String encrypt(Key key, String inputFileName, String outputFileName) {
        return doCrypto(Cipher.ENCRYPT_MODE, key, inputFileName, outputFileName);
    }

    public static String decrypt(Key key, String inputFileName, String outputFileName) {
        return doCrypto(Cipher.DECRYPT_MODE, key, inputFileName, outputFileName);
    }

    private static String doCrypto(int cipherMode, Key key, String inputFileName, String outputFileName) {
        try {
            if(inputFileName.equals("")) {
                return null;
            }

            File inputFile = new File("files/" + inputFileName);
            File outputFile = new File("files/" + outputFileName);

            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(cipherMode, key);

            FileInputStream inputStream = new FileInputStream(inputFile);
            byte[] inputBytes = new byte[(int) inputFile.length()];
            inputStream.read(inputBytes);

            byte[] outputBytes = cipher.doFinal(inputBytes);

            FileOutputStream outputStream = new FileOutputStream(outputFile);
            outputStream.write(outputBytes);

            inputStream.close();
            outputStream.close();

            return outputFile.getPath();
        } catch (NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException | IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
